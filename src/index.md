---
layout: "layouts/default.njk"
title: "Frontpage"
eleventyNavigation:
  key: Frontpage
  title: Home
  order: 1
---


## Front page
Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor asperiores est omnis voluptatibus? Quibusdam aut sed, odit quis perferendis nostrum et rem dicta itaque iure, hic libero nesciunt qui facere. Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci perspiciatis quos nam distinctio accusamus excepturi quo quasi mollitia necessitatibus maxime tempora minus voluptatem, possimus sapiente magni impedit laboriosam facere sunt.  

![Cat](/assets/images/June_odd-eyed-cat_cropped.jpg)

<div class="Grid TwoColumns">
  <div class="Grid-cell">
    <h3>Column #1</h3>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor asperiores est omnis voluptatibus? Quibusdam aut sed, odit quis perferendis nostrum et rem dicta itaque iure, hic libero nesciunt qui facere.</p>
  </div>
  <div class="Grid-cell">
    <h3>Column #2</h3>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor asperiores est omnis voluptatibus? Quibusdam aut sed, odit quis perferendis nostrum et rem dicta itaque iure, hic libero nesciunt qui facere.</p>
  </div>  
</div>