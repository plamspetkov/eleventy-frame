---
title: 'Dandelion'
permalink: '/flower/{{ title | slugify }}/'
imageSrc: 'the-witcher-dandelion-jaskier-custom-header.avif'
bloomTime: 'when Geralt is angry'
date: 2020-05-20
eleventyExcludeFromCollections: false
---

![Dandelion](/assets/images/the-witcher-dandelion-jaskier-custom-header.avif)

Lorem Ipsum. This is just pure markdown!
